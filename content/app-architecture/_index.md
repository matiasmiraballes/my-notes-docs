+++
title = "App Architecture"
chapter = false
weight = 30
pre = "<b>3. </b>"
+++

## Breaking Down Components

The application will be composed of 3 kinds of components, page, container and Presentational Components.

### Page Components

Page components are pretty self-explanatory, page components are the routed components that define the overall layout of the page, these serve as a container for the smart components. Page components have little to no business logic.

### Container and Presentational Components

One common pattern used in Angular is the separation of components into **container** and **presentational components**, also known as **smart and dumb components**, respectively.

Smart components are responsible for handling the business logic and consume data from the state layer of the application, passing the data down to the dumb components to render the application.

Dumb components on the other hand, should only interact through its inputs and outputs, keeping them agnostic of who its parent component is as long as it can supply the necessary inputs. Personally, I believe dumb components may or may not interact with DI as long as it doesn’t generate side effects -dumb components for example should not trigger navigation or interact the state layer, but using a service to autocomplete or validate a form field could be fine.
This approach has several benefits:
-	Helps to better segregate responsibilities and break down components into smaller pieces.
-	The heavy use of dumb components leads to more reusable components
-	Can add performance benefits when using the OnPush change detection strategy

## Back to the application

Now that we know what about the different types of components of our application, we can take another look at the modules folder of the folder structure diagram.

```text
├───src
│   ├───app
│   │   ├─── [+] core
│   │   ├─── [+] root 
│   │   ├───modules
│   │   │   ├───<feature>
│   │   │   │   ├───components
│   │   │   │   │   ├─── [+] <smart-component-1>
│   │   │   │   │   ├─── [+] <smart-component-2>
│   │   │   │   │   └───ui
│   │   │   │   │       ├─── [+] <presentation-component-1>
│   │   │   │   │       └─── [+] <presentation-component-2>
│   │   │   │   ├───page                            
│   │   │   │   │   └───<feature>.component.ts            
│   │   │   │   └───<feature>.module.ts
│   │   │   └─── [+] shared 
```

As you can see, inside the modules folder we create a folder for each feature. Each feature has its now module where we should declare all of the components and routes if necessary. 

We have a single page component that represents the root of the feature.

We have a **components folder** that holds all the smart components, and a **ui folder** for the presentation components.

Any component that is reused by multiple modules can be moved to the **shared folder**.

## Application Diagram

Here is a diagram of the main components used in the application. As you can see, following this pattern makes it easy to support scenarios in which parts of the application have to react to changes in other parts. Changes in the stores will cause the affected branches of the application to rerender and start from a clean state. This pattern also helps to avoid problems like data duplication caused by not resetting internal state variables of child components.

![Application Diagram](app-architecture-1.png)

For reference, here is a screenshot of the application with the smart components highlighted.

![Application Screenshot](app-architecture-2.jpg)