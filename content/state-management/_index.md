+++
title = "State Management"
chapter = false
weight = 50
pre = "<b>5. </b>"
+++

## First of all, what is state management?

State management is the implementation of a design pattern that helps us synchronize the state across the application. This is generally done by centralizing the data consumed by the application in a single place. Adding a state management layer has several benefits:

- By having a single source of truth, we can easily synchronize the data consumed by components on the page or even throughout the application, even if they don’t necessarily have a parent-child relationship.
- Having the logic for state management in a centralized place also gives us a convenient location to implement code that reacts to data fetching, such as error handling and loading states, as well as making the code cleaner and easier to understand.
- Decoupling the actions of fetching and reading data allows us to consume this data in a more reactive manner. It also reduces the number of HTTP requests to the back-end.

## Implementing the “data source options” requirement

One of the features I want to have in the application is _users must be able to choose the source of data from a dropdown list and set modifiers to the HTTP request to the backend, such as adding an artificial delay or chances of failure_. As the data is not replicated in different sources, when the user selects a different source, the application needs to re-fetch the data that should be displayed. The State-Management layer can help listen for changes on these options and reload the data when necessary. The State-Management layer can help listen for changes on these options and reload the data when necessary.

## How to implement the State-Management

There are various solutions for implementing state management in an application, with libraries such as NgRx or its more lightweight version, @ngrx/component-store. However, for our small application, we will use **services with a subject**. This approach will meet our requirements while keeping the implementation relatively simple.

## Implementing the Service with a Subject approach

Implementing the simplest version of the pattern is as easy as adding a `Subject` to hold the state, initialize it either in the constructor or the first time someone calls the getNotes method, expose an observable of the Subject we just created.

```typescript
@Injectable()
export class NotesStore {
  // Set initial default state
  private notesSubject: BehaviorSubject<Note[]> = new BehaviorSubject([]);

  constructor(private notesService: NotesService) {
    this.initialize();
  }

  private initialize() {
    this.getNotesEffect();
  }

  public getNotes$: Observable<Note[]> = this.notesSubject.asObservable();

  private getNotesEffect() {
    this.notesService
      .getNotes()
      .subscribe((notes) => this.notesSubject.next(notes));
  }
}
```

Additionally we can keep track of the store state, so components can react to loading and error states.

```typescript
@Injectable()
export class NotesStore {
  private notesSubject: BehaviorSubject<Note[]> = new BehaviorSubject([]);
  private statusSubject: BehaviorSubject<StoreInfo> =
    new BehaviorSubject<StoreInfo>({
      status: "NotStarted",
      error: null,
    });

  constructor(private notesService: NotesService) {
    this.initialize();
  }

  private initialize() {
    this.emitStatus("Loading", null);
    this.getNotesEffect();
  }

  public getNotes$: Observable<Note[]> = this.notesSubject.asObservable();
  public storeStatus$ = this.statusSubject.asObservable();

  private getNotesEffect() {
    this.notesService.getNotes().subscribe({
      next: (notes) => {
        this.notesSubject.next(notes), this.emitStatus("Initialized", null);
      },
      error: (e: Error) => {
        // If fails, the store is set to error status until it is able to successfully retrieve data.
        this.emitStatus("Error", e);

        // The global error handler takes care of notifying the error to the user.
        // The FrienlyError class allow us to re-throw the error with a friendly message
        // while keeping the original message and stack trace intact.
        throw new FriendlyError(e, "Something failed while loading notes.");
      },
      complete: () => {},
    });
  }

  // Shorthand for emitting new store status, passing null as status argument will leave the current value
  private emitStatus(status: StoreStatus, error: Error) {
    status ??= this.statusSubject.value.status;
    this.statusSubject.next({
      status: status,
      error: error,
    });
  }
}
```

Notice that the `@Injectable()` decorator is missing the `providedIn` attribute, this is intentional. By not declaring the scope where the service should provided neither in the decorator nor in any module, we can instruct Angular that instead of using a singleton instance, it should provide the component with an fresh instance of it, and dispose the instance when nobody is using it. Angular will reuse any active instance when trying to provide this dependency to a component, meaning that we can share the instance of the service between multiple components active in the same page.

We can accomplish it by using the `providers` property of the smart components.

```typescript
@Component({
  selector: 'notes-collection',
  providers: [NotesStore],
  (...),
})
export class NotesCollectionComponent {
  (...)
}
```

##### [ADD EXPLANATION FOR ACTIONS, EFFECTS AND REDUCERS]
##### [ADD STEPS IN-BETWEEN FOR HANDLING REQUEST OPTIONS AND PAGINATION]
##### [MOVE GENERIC LOGIC TO ABSTRACT CLASS]

Now, this is how the final implementation looks like:

### Abstract Store

```typescript
import { Injectable } from '@angular/core'
import { BehaviorSubject, combineLatest, map, Observable } from 'rxjs'
import { RequestOptionsBuilder } from './interfaces/request-options'
import { StoreInfo, StoreStatus } from './interfaces/store-info'

@Injectable()
export abstract class AbstractStore {
  private statusSubject: BehaviorSubject<StoreInfo> =
    new BehaviorSubject<StoreInfo>({
      status: 'NotStarted',
      error: null,
    })
  storeStatus$ = this.statusSubject.asObservable()

  private filterSubject = new BehaviorSubject<string>('')
  private currentPageSubject = new BehaviorSubject<number>(1)
  private pageSizeSubject = new BehaviorSubject<number>(7)
  protected totalItemsSubject = new BehaviorSubject<number>(0)

  private totalPages$: Observable<number> = combineLatest([
    this.pageSizeSubject,
    this.totalItemsSubject,
  ]).pipe(map(([pageSize, itemCount]) => Math.ceil(itemCount / pageSize)))

  private requestOptions$: Observable<RequestOptionsBuilder> = combineLatest([
    this.currentPageSubject,
    this.pageSizeSubject,
    this.filterSubject,
  ]).pipe(
    map(([page, pageSize, filter]) => {
      return new RequestOptionsBuilder(pageSize, (page - 1) * pageSize, filter)
    })
  )

  options = {
    get: {
      page$: this.currentPageSubject.asObservable(),
      pageSize$: this.pageSizeSubject.asObservable(),
      totalItems$: this.totalItemsSubject.asObservable(),
      filter$: this.filterSubject.asObservable(),
      totalPages$: this.totalPages$,
      requestOptions$: this.requestOptions$,
    },
    set: {
      page: (page: number) => this.currentPageSubject.next(page),
      totalItems: (itemCount: number) => {
        this.totalItemsSubject.next(itemCount)
      },
      filter: (searchTerm: string) => this.filterSubject.next(searchTerm),
      pageSize: (pageSize: number) => this.pageSizeSubject.next(pageSize),
    },
  }

  // Shorthand for emitting new store status, passing null as status argument will leave the current value
  protected emitStatus(status: StoreStatus, error: Error) {
    status ??= this.statusSubject.value.status
    this.statusSubject.next({
      status: status,
      error: error,
    })
  }
}
```

### Notes Store

```typescript
import { Injectable, OnDestroy } from '@angular/core'
import { ToastrService } from 'ngx-toastr'
import {
  BehaviorSubject,
  combineLatest,
  Observable,
  switchMap,
  take,
} from 'rxjs'
import { SubSink } from 'subsink'
import { FriendlyError } from '../error-handling/friendly-error'
import { NotesService } from '../http/services/notes.service'
import { Note } from '../models/note.model'
import { DataSourceSettings } from '../settings/models/data-source-settings.model'
import { SettingsStore } from '../settings/settings.store'
import { AbstractStore } from './abstract-store'
import { RequestOptionsBuilder } from './interfaces/request-options'

@Injectable()
export class NotesStore extends AbstractStore implements OnDestroy {
  private subs = new SubSink()

  // Set initial default state
  private notesSubject: BehaviorSubject<Note[]> = new BehaviorSubject([])

  constructor(
    private notesService: NotesService,
    private toastService: ToastrService,
    private settingsStore: SettingsStore
  ) {
    super()
    this.subs.sink = this.notesSubject
      .pipe(switchMap(_ => this.options.get.requestOptions$))
      .subscribe(options => this.updateNoteCount(options))

    this.subs.sink = settingsStore.dataSourceSettings$.subscribe(
      (dataSourceSettings: DataSourceSettings) => {
        this.initialize()
      }
    )
  }

  private updateNoteCount(options) {
    this.notesService.getNotesCount(options).subscribe(count => {
      this.totalItemsSubject.next(count)
    })
  }

  private initialize() {
    this.emitStatus('Loading', null)
    this.getNotesEffect()
  }

  /**
   * Return notes from the store.
   *
   * @type {Observable<Note[]>}
   */
  public getNotes(): Observable<Note[]> {
    return this.notesSubject.asObservable()
  }

  /**
   * Updates notes and updates the state in the store.
   *
   * @type {Observable<Note[]>}
   */
  public createOrUpdateNote(note: Note) {
    this.createOrUpdateNoteEffect(note)
  }

  /**
   * Deletes a note and updates the state in the store.
   *
   * @type {Observable<Note[]>}
   */
  public deleteNote(note: Note) {
    this.deleteNoteEffect(note)
  }

  // #region Effects
  // Effects take care of interacting with the data sources to get/update the data
  // and update the current state (notesSubject) with the results

  // Reducers are also included here, implicit inside the 'next:' functions
  // Reducers are the functions that takes the new piece of data as parameter and returns the updated state

  private getNotesEffect() {
    this.options.get.requestOptions$
      .pipe(
        switchMap(requestOptions =>
          this.notesService.getNotes(requestOptions.asHttpParams())
        )
      )
      .subscribe({
        next: notes => {
          this.notesSubject.next(notes), this.emitStatus('Initialized', null)
        },
        error: (e: Error) => {
          // If fails, the store is set to error status until it is able to successfully retrieve data.
          this.emitStatus('Error', e)

          // The global error handler takes care of notifying the error to the user.
          // The FrienlyError class allow us to re-throw the error with a friendly message
          // while keeping the original message and stack trace intact.
          throw new FriendlyError(e, 'Something failed while loading notes.')
        },
        complete: () => {},
      })
  }

  private createOrUpdateNoteEffect(note: Note) {
    this.emitStatus('Updating', null)
    combineLatest([
      this.notesService.createOrUpdateNote(note),
      this.options.get.requestOptions$,
    ])
      .pipe(take(1))
      .subscribe({
        next: ([createdOrUpdatedNote, requestOptions]) => {
          this.notesSubject.next(
            this.applyFilters(
              [
                createdOrUpdatedNote,
                ...this.notesSubject.value.filter(
                  n => n.id != createdOrUpdatedNote.id
                ),
              ],
              requestOptions
            )
          )
          this.toastService.success('Notes Updated!')
        },
        error: e => {
          // Failing to update notes doesn't lead to a critical failure state
          // so we notify the error and leave the store status as is.
          this.emitStatus(null, e)

          // The global error handler takes care of notifying the error to the user.
          // For this We can re-throw the error with a friendly message.
          throw new FriendlyError(e, 'Something failed while updating notes.')
        },
        complete: () => {
          this.getNotesEffect()
        },
      })
  }

  private deleteNoteEffect(note: Note) {
    this.emitStatus('Updating', null)
    this.notesService.deleteNote(note).subscribe({
      next: () => {
        this.notesSubject.next([
          ...this.notesSubject.value.filter(n => n.id != note.id),
        ])
        this.toastService.success('Note Deleted!')
      },
      error: (e: Error) => {
        // If fails, the store is set to error status until it is able to successfully retrieve data.
        this.emitStatus('Error', e)

        // The global error handler takes care of notifying the error to the user.
        // The FrienlyError class allow us to re-throw the error with a friendly message
        // while keeping the original message and stack trace intact.
        throw new FriendlyError(e, 'Something failed while deleting notes.')
      },
      complete: () => {
        this.getNotesEffect()
      },
    })
  }

  //#endregion

  private applyFilters(
    notes: Note[],
    filterOptions: RequestOptionsBuilder
  ): Note[] {
    let options = filterOptions.asObject()
    return notes
      .filter(
        note =>
          note.title
            .toLocaleLowerCase()
            .includes(options.filter.toLocaleLowerCase()) ||
          note.description
            .toLocaleLowerCase()
            .includes(options.filter.toLocaleLowerCase())
      )
      .slice(0, options.limit)
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }
}
```

### Request Options

```typescript
import { HttpParams } from '@angular/common/http'

export interface RequestOptions {
  limit: number
  offset: number
  filter: string
}

export enum RequestOptionsEnum {
  'limit' = 'limit',
  'offset' = 'offset',
  'filter' = 'filter',
}

export class RequestOptionsBuilder {
  limit: number
  offset: number
  filter: string

  constructor(limit: number, offset: number, filter: string) {
    this.limit = limit
    this.offset = offset
    this.filter = filter
  }

  asHttpParams() {
    return new HttpParams()
      .set(RequestOptionsEnum.limit, this.limit)
      .set(RequestOptionsEnum.offset, this.offset)
      .set(RequestOptionsEnum.filter, this.filter)
  }

  asObject(): RequestOptions {
    return {
      limit: this.limit,
      offset: this.offset,
      filter: this.filter,
    }
  }
}
```

## Unidirectional Data Flow Pattern

Unidirectional Data Flow is an architectural pattern where the flow of data in an application is one-way, meaning that data is passed in a single direction through a set of components or functions.

In a unidirectional dataflow architecture, the data flows from a parent component down to its child components, and any changes to the data must flow back up the component tree through a series of well-defined events or actions. This ensures that there is a clear and predictable flow of data, and that changes to the state of the application are easily traceable.
