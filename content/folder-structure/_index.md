+++
title = "Folder Structure"
chapter = false
weight = 20
pre = "<b>2. </b>"
+++

When working on a large project, having a defined way to organize your solution is extremely helpful for keeping the codebase clean and maintainable. With that said, there are no blueprints here that fits perfectly on every project.
We will start by defining a basic folder configuration, that seems to scale well, and make some tweaks as necessary as the project grows.

## High Level Structure

We will have a `src` folder for all the files related to the application code, that way we don't mix them with the files for Angular configuration. Additionally we have a cypress folder for the E2E tests and E2E test configuration.

Inside the **app** folder we create 3 folders: core, modules and root.

Unit tests are located next to the element being unit tested.

```text
├─── [+] cypress
├───src
│   ├───app
│   │   ├─── [+] core
│   │   ├─── [+] modules
│   │   ├─── [+] root 
│   ├─── [+] assets
│   └─── [+] environments
```

### Core

There we will contain all the modules that are used across the application, like services, interceptors, guards, models, etc.

```text
├───src
│   ├───app
│   │   ├─── [+] modules
│   │   ├─── [+] root
│   │   ├───core
│   │   │   ├─── [+] auth
│   │   │   ├─── [+] error-handling
│   │   │   ├───mocks
│   │   │   │   ├─── [+] data
│   │   │   │   └───mock-backend.service.ts
│   │   │   ├─── [+] settings
│   │   │   ├─── [+] stores
│   │   │   ├─── [+] utils
│   │   │   ├─── [+] (...)
│   │   │   ├───http
│   │   │   │   ├─── [+] services
│   │   │   │   ├───body  
│   │   │   │   │   └───<feature>
│   │   │   │   │       └───<req-name>.body.ts
│   │   │   │   └───response
│   │   │   │       └───<feature>
│   │   │   │           └───<req-name>.response.ts
│   │   │   ├─── [+] models
│   │   │   ├───app.routing.module.ts
│   │   │   └───app.module.ts
```

There are two ways in which we could group the files inside the core folder.

1. **horizontally**: create a folder for each **file type**.
2. **vertically**: create a folder for each module or domain. In other words, place all the elements with a shared responsability in the same folder.

Number one is easier to do and works fine for small projects, but once the codebase starts to grow, it gets harder and harder to find related elements, leading to code duplication and harder times debugging sections of the code that you are not familiar with, so we will try to apply the second option whenever posible.

![folder-structure-1.png](folder-structure-1.png)

Most of the folders shown in the example are pretty self explanatory, is worth mentioning that the http and models folder could be further broken down in a vertical way, but I think this project is small enough that just having a folder for services that make HTTP calls to the backend, another exclusively for the DTOs (body and responses) and one for entities of the application (models) is enough.

### Root

Angular requires one module to be loaded as the application starts. We call this as root module. The root module loads the root component and all other modules.
The root module is conventionally called as AppModule and is created under the `/src/app` folder.

```text
├───src
│   ├───app
│   │   ├─── [+] core
│   │   ├─── [+] modules
│   │   └───root
│   │       ├─── [+] layout                         
│   │       └───app.component.ts
```

### Modules

The modules folder will store all of the **feature modules**. Each feature will often represent a page or routed component. As you can see in the diagram, we mention smart components, presentation components and pages, we will talk about them in the next topic.

```text
├───src
│   ├───app
│   │   ├─── [+] core
│   │   ├─── [+] root 
│   │   ├───modules
│   │   │   ├───<feature>
│   │   │   │   ├───components
│   │   │   │   │   ├─── [+] <smart-component-1>
│   │   │   │   │   ├─── [+] <smart-component-2>
│   │   │   │   │   └───ui
│   │   │   │   │       ├─── [+] <presentation-component-1>
│   │   │   │   │       └─── [+] <presentation-component-2>
│   │   │   │   ├───page                            
│   │   │   │   │   └───<feature>.component.ts            
│   │   │   │   └───<feature>.module.ts
│   │   │   └─── [+] shared 
```

<u>Related concepts</u>: Domain Driven Design, Domain Driven Folder Structure