+++
title = "Introduction"
chapter = false
weight = 10
pre = "<b>1. </b>"
url = '/'
+++

# INTRODUCTION

## Why MyNotes is how it is?

If you visited the site, you may have noticed that some of the decisions don’t a ton of sense. Who needs to switch databases on the fly when the only thing they want to do is write some notes. The answer is simple, really. I just wanted to make a website to practice and learn some Angular concepts. One of the concepts that I wanted to practice is Reactive Programming, get some hands-on experience to get a grasp of the advantages, the gotchas and how not to fall into them, and what could be better that an app where every piece of data could change with the press of a button. OK, with that out of the way, let’s continue.

## The requirements

The functional requirements of the application are quite simple:
-   Authenticated users should be able to:
      -	write notes
      -	share notes to other users
      -	choose the source of data from a dropdown list and set modifiers to the HTTP request to the backend, such as adding an artificial delay or chances of failure

## Application Diagram / Tech Stack

![Tech stack diagram](stack-diagram.png)

## Why HUGO?

The DOCS section consists mainly of content and does not require complex business logic. Instead of using a full-blown Single Page Application (SPA) framework, we can utilize a Static Website Generator to generate this section.

Hugo offers several advantages:

-  Simplicity: Once the initial setup is done, we only need to write markdown files, and Hugo will take care of generating the corresponding HTML files.
-  Speed: Serving static files provides a significant performance boost.
By leveraging Hugo, we can streamline the development process, focusing on writing content using markdown files while benefiting from the speed and simplicity of a Static Website Generator.

## The app

![website-image.jpg](website-image.jpg?height=500px)

### And some e2e footage

{{< video src="./e2e-test.mp4" type="video/mp4" preload="auto" >}}
