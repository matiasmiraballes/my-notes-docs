+++
title = "Change Detection Strategies"
chapter = false
weight = 40
pre = "<b>4. </b>"
+++

There are currently two change detection strategies in Angular, with a third possibly on the way. Let's focus on the existing two for now: the **Default** (CheckAlways) and **OnPush** (CheckOnce) strategies. Before diving deeper into these strategies, it's important to have a high-level understanding of how change detection works in Angular. 

If you want to read a more detailed explanation of this topic, I recommend checking out this [article by Brilyan Aro](https://blogs.halodoc.io/understanding-angular-change-detection-strategy/#:~:text=By%20default%2C%20angular%20will%20run,()%20has%20a%20new%20reference.) and this post from the [Angular University Blog](https://blog.angular-university.io/how-does-angular-2-change-detection-really-work/). In summary, change detection in Angular involves detecting and propagating changes to the application's state and view.

#### Change detection steps:

1.	**NgZone triggers a change detection run** when one of the following events happen
    - Browser events (click, mouseover, etc)
    - Ajax calls
    - Timer’s tick (setTimeout, setInterval)
2.	When a change detection run is triggered, **Angular traverses the component tree** from top to bottom. The change detectors of each component decides whether the component should scan for changes on its model or not based on their Change Detection Strategy. By scan for changes, we mean if it should re-calculate all the variables and functions bonded to the view of the component and compare the new values with the current values of the model.
    - **Default change detection strategy**: Always scan for changes
    - **OnPush change detection strategy**: Only scan for changes for this component or its childrens when at least one of the following conditions are met:
      - The **reference** of any @input or @output of the component has changed.
      - The event as triggered from the component or one of its children.
      - The component is marked for check by calling **ChangeDetectorRef.markForCheck()**.
      - The **async pipe** was executed (which calls markForCheck() under the hood)
3.	If any of the previous mentioned conditions were met, the change detector re-evaluates the properties used in the template of the component (the model), if the current values and the previous values are different, the component is marked as changed.

## OnPush vs Default

As you can imagine by the previous explanation, the Default strategy always checks for changes after every asynchronous event, making it more reliable at finding changes, especially for newcomers to the framework that may not fully grasp how the change detection mechanism works on Angular and fall into the pitfalls of the OnPush strategy.

If the Default strategy is easier to use, why would I ever use the OnPush strategy? Well, the obvious benefit is that the convenience of the Default strategy comes with a cost on performance, by reducing the amount of events that can trigger a change detection cycle, and reducing the amount of branches of the component tree that will be evaluated, we can provide a smoother experience when it comes to rendering changes in the app. At the same time, Angular team worked very hard to optimize how the calculations for change detection are performed, so we won’t notice any real difference unless we are working with a huge page loaded with many, MANY components or components that loads a lot of data.

The main and not so obvious advantage of using OnPush strategy is that this enforces practices that lead to better architecture apps. To give an example, by removing some of the asynchronous events from the list, like timers and subscriptions that updates attributes directly, we are encouraged to rely less on “side-effects” and handle these cases in a cleaner way, like using the async pipe, which not only is more readable, also handles unsubscribing from the observables for us. And having the change of @inputs/@outputs as one of the main means for triggering change detection leads to designing components in a more modular fashion. 
